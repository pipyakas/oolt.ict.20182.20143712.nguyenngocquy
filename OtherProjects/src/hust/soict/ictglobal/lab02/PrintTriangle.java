package hust.soict.ictglobal.lab02;

import java.util.Scanner;

public class PrintTriangle {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter n? ");
		int n = keyboard.nextInt();
		for (int i = 0; i < n; i++) {
			for (int j = n - i; j > 1; j--) {
				System.out.print(" ");
			}
			for (int j = 0; j <= i; j++) {
				System.out.print("* ");
			}
			System.out.println();
		}
		keyboard.close();
	}

}
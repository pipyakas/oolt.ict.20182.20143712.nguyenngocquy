package hust.soict.ictglobal.lab02;

import java.util.Arrays;

public class ArraySortSum {
	public static void main(String[] args) {

		int[] array1 = { 1789, 2035, 1899, 1456, 2013, 1458, 2458, 1254, 1472, 2365, 1456, 2165, 1457, 2456 };

		int sum = 0;
		for (int i = 0; i < array1.length; i++)
			sum = sum + array1[i];

		System.out.println("Original numeric array : " + Arrays.toString(array1));
		Arrays.sort(array1);
		System.out.println("Sorted numeric array : " + Arrays.toString(array1));
		System.out.print("Sum of all array elements: " + sum);
	}
}
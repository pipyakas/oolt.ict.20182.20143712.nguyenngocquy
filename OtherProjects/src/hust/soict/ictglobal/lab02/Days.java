package hust.soict.ictglobal.lab02;

import java.util.Scanner;

public class Days {

	public static void main(String[] strings) {

		Scanner input = new Scanner(System.in);

		int days = 0;
		String month = "Unknown";

		System.out.print("Enter month (1-12): ");
		while (!input.hasNextInt()) {
			System.out.print("Please enter a number\nEnter month (1-12): ");
			input.next();
		}

		int mm = input.nextInt();
		while (mm > 12 || mm < 1) {
			System.out.print("Invalid value. Re-enter: ");
			mm = input.nextInt();
		}

		System.out.print("Enter year: ");
		while (!input.hasNextInt()) {
			System.out.print("Please enter a number\nEnter year: ");
			input.next();
		}
		int year = input.nextInt();

		switch (mm) {
		case 1:
			month = "January";
			days = 31;
			break;
		case 2:
			month = "February";
			if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
				days = 29;
			} else {
				days = 28;
			}
			break;
		case 3:
			month = "March";
			days = 31;
			break;
		case 4:
			month = "April";
			days = 30;
			break;
		case 5:
			month = "May";
			days = 31;
			break;
		case 6:
			month = "June";
			days = 30;
			break;
		case 7:
			month = "July";
			days = 31;
			break;
		case 8:
			month = "August";
			days = 31;
			break;
		case 9:
			month = "September";
			days = 30;
			break;
		case 10:
			month = "October";
			days = 31;
			break;
		case 11:
			month = "November";
			days = 30;
			break;
		case 12:
			month = "December";
			days = 31;
		}
		System.out.print(month + " " + year + " has " + days + " days\n");
		input.close();
	}
}
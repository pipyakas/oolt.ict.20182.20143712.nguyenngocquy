package hust.soict.ictglobal.lab02;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

/**
 * Simple class to determine the number of days in a given month with java
 * utility classes.
 * 
 * @author shartman
 *
 */
public class Days2 implements Runnable {

	/**
	 * Main method.
	 */
	public static void main(String[] args) {
		new Days2().run();
	}

	@Override
	public void run() {
		Scanner scanner = new Scanner(System.in);
		int year;
		int month;

		System.out.println("Enter the month number (1-12): ");
		month = scanner.nextInt();
		System.out.println("Enter the year number: ");
		year = scanner.nextInt();
		scanner.close();

		System.out.println("The number of days in month " + month + " in year " + year + " is: "
				+ determineNumberOfDays(year, month));
	}

	/**
	 * @param year  The year to get the number of days for the given month.
	 * @param month The month number to get the days for. Caveat this goes from 1 to
	 *              12 instead of 0 to 11 as used in {@link Calendar}.
	 * @return The number of days in the given year and month.
	 * @throws IllegalArgumentException if the month is not in range from 1 to 12.
	 *                                  Year is not checked.
	 */
	private int determineNumberOfDays(int year, int month) {
		GregorianCalendar calendar = new GregorianCalendar();

		if ((month < 1) || (month > 12)) {
			throw new IllegalArgumentException(
					"Given month number " + month + " is not in the accepted range from 1 to 12");
		}

		calendar.set(Calendar.YEAR, year);
		// Month set to month -1 because we asked to input between 1 and 12.
		calendar.set(Calendar.MONTH, month - 1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);

		return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	}
}
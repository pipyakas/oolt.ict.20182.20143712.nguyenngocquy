package hust.soict.ictglobal.lab02;

import java.util.Scanner;

public class AddMatrices {
	public static void main(String args[]) {
		int c, d;
		Scanner input = new Scanner(System.in);

		System.out.print("Enter number of rows for both matrix: ");
		while (!input.hasNextInt()) {
			System.out.print("Please enter a number\nEnter number of rows for both matrix: ");
			input.next();
		}
		int m = input.nextInt();
		System.out.print("Enter number of columns for both matrix: ");
		while (!input.hasNextInt()) {
			System.out.print("Please enter a number\nEnter number of columns for both matrix: ");
			input.next();
		}
		int n = input.nextInt();

		int array1[][] = new int[m][n];
		int array2[][] = new int[m][n];
		int sum[][] = new int[m][n];

		System.out.println("Enter elements of the first matrix: ");
		for (c = 0; c < m; c++)
			for (d = 0; d < n; d++) {
				while (!input.hasNextInt()) {
					System.out.print("Please enter a number\n");
					input.next();
				}
				array1[c][d] = input.nextInt();
			}

		System.out.println("Enter elements of the second matrix: ");
		for (c = 0; c < m; c++)
			for (d = 0; d < n; d++) {
				while (!input.hasNextInt()) {
					System.out.print("Please enter a number\n");
					input.next();
				}
				array2[c][d] = input.nextInt();
			}

		for (c = 0; c < m; c++)
			for (d = 0; d < n; d++)
				sum[c][d] = array1[c][d] + array2[c][d];

		System.out.println("Sum of the two matrices: ");

		for (c = 0; c < m; c++) {
			for (d = 0; d < n; d++)
				System.out.print(sum[c][d] + "\t");
			System.out.println();
		}
		input.close();
	}
}
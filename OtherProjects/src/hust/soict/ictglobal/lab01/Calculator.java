package hust.soict.ictglobal.lab01;

import java.util.Scanner;

public class Calculator {
	public static void main(String[] args) {
		int opt;
		double m, n, add, sub, mul, div;
		Scanner s = new Scanner(System.in);
		System.out.print("Enter first number:");
		m = s.nextDouble();
		System.out.print("Enter second number:");
		n = s.nextDouble();
		while (true) {
			System.out.println("Enter 1 for sum");
			System.out.println("Enter 2 for difference");
			System.out.println("Enter 3 for product");
			System.out.println("Enter 4 for quotient");
			System.out.println("Enter 5 to Exit");
			opt = s.nextInt();
			switch (opt) {
			case 1:
				add = m + n;
				System.out.println("Result:" + add);
				break;

			case 2:
				sub = m - n;
				System.out.println("Result:" + sub);
				break;

			case 3:
				mul = m * n;
				System.out.println("Result:" + mul);
				break;

			case 4:
				div = (double) m / n;
				System.out.println("Result:" + div);
				break;

			case 5:
				System.exit(0);
			}
			s.close();
		}

	}

}
package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;

import hust.soict.ictglobal.aims.Aims.PlayerException;

public class CompactDisc extends Disc implements Playable, Comparable<CompactDisc> {
	private String artist;
	private int length;
	ArrayList<Track> tracks = new ArrayList<Track>();

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public void addTrack(Track track) {
		int check = 0;
		for (Track name : tracks) {
			if (name == track) {
				check = 1;
				System.out.println("Already in the list!");
			}
		}
		if (check == 0) {
			tracks.add(track);
			System.out.println("Added track!");
		}
	}

	public void removeTrack(Track track) {
		int check = 0;
		for (Track name : tracks) {
			if (name == track) {
				check = 1;
			}
		}
		if (check == 1) {
			tracks.remove(track);
			System.out.println("Removed!");
		} else {
			System.out.println("Cannot find " + track + "!");
		}
	}

	public int getLength() {
		int totalLength = 0;
		for (Track name : tracks) {
			totalLength += name.getLength();
		}
		return totalLength;
	}

	public void play() throws PlayerException {
		if (this.getLength() <= 0) {
			System.err.println("Error: CD length is 0");
			throw (new PlayerException());
		}
		System.out.println("Playing CD: " + this.getTitle());
		System.out.println("Total length: " + this.getLength());
		java.util.Iterator itrt = tracks.iterator();
		Track nextTrack;
		while (itrt.hasNext()) {
			nextTrack = (Track) itrt.next();
			try {
				nextTrack.play();
			} catch (PlayerException exp) {
				exp.printStackTrace();
			}
		}
	}

	public int compareTo(CompactDisc track2) {
		int track1 = Integer.compare(this.tracks.size(), track2.tracks.size());
		return (track1 != 0) ? track1 : Integer.compare(this.getLength(), track2.getLength());
	}
}

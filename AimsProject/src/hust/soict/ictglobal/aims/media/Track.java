package hust.soict.ictglobal.aims.media;

import hust.soict.ictglobal.aims.Aims.PlayerException;

public class Track implements Playable, Comparable<Track> {
	private String title;
	private int length;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public void play() throws PlayerException {
		if (this.getLength() <= 0) {
			System.err.println("Error: DVD length is 0");
			throw (new PlayerException());
		}
		System.out.println("Playing track: " + this.getTitle());
		System.out.println("Track length: " + this.getLength());
	}

	public int compareTo(Track track2) {
		return this.getTitle().compareTo(track2.getTitle());
	}
}

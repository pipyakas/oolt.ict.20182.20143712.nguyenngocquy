package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Book extends Media implements Comparable<Book> {

	private boolean check = false;
	private List<String> authors = new ArrayList<String>();

	public Book() {
		// TODO Auto-generated constructor stub
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	public void addAuthor(String authorName) {
		for (String name : authors) {
			if (name.equals(authorName)) {
				check = true;
				System.out.println("Author " + name + "is already in the list.");
			}
		}
		if (check == false) {
			authors.add(authorName);
			System.out.println("Author " + authorName + " added.");
		}
	}

	public void removeAuthor(String authorName) {
		for (String name : authors) {
			if (name.equals(authorName)) {
				authors.remove(authorName);
				check = true;
				System.out.println("Author " + authorName + " removed.");
			}
		}
		if (check == false) {
			System.out.println("Author " + authorName + " not found.");
		}
	}

	public int compareTo(Book book2) {
		return this.getTitle().compareTo(book2.getTitle());
	}

}

package hust.soict.ictglobal.aims.media;

import hust.soict.ictglobal.aims.Aims.PlayerException;

public class DigitalVideoDisc extends Disc implements Playable, Comparable<DigitalVideoDisc> {
	private String director;
	private int length;

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public DigitalVideoDisc(String title) {
		setTitle(title);
	}

	public DigitalVideoDisc(String title, String category) {
		setTitle(title);
		setCategory(category);
	}

	public DigitalVideoDisc(String title, String category, String director) {
		setTitle(title);
		setCategory(category);
		this.director = director;
	}

	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		setCategory(category);
		setTitle(title);
		this.director = director;
		this.length = length;
		setCost(cost);
	}

	public boolean search(String title) {
		int name1 = 0;
		int name2 = 0;
		String[] name = title.split(" ");
		for (String c : name) {
			name1++;
			if (this.getTitle().toLowerCase().indexOf(c.toLowerCase()) != -1) {
				name2++;
			}
		}
		return (name2 == name1) ? true : false;
	}

	public void play() throws PlayerException {
		if (this.getLength() <= 0) {
			System.err.println("Error: DVD length is 0");
			throw (new PlayerException());
		}
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}

	public int compareTo(DigitalVideoDisc dvd2) {
		if (dvd2 instanceof DigitalVideoDisc) {
			return Float.compare(this.getCost(), dvd2.getCost());
		} else {
			DigitalVideoDisc newdvd = (DigitalVideoDisc) dvd2;
			return Float.compare(this.getCost(), newdvd.getCost());
		}
	}
}

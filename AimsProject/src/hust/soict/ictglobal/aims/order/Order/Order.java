package hust.soict.ictglobal.aims.order.Order;

import java.util.ArrayList;
import java.util.Random;

import hust.soict.ictglobal.aims.media.Media;

public class Order extends Media {
	public static final int MAX_NUMBER_ORDERED = 10;
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	private int luckyIndex;
	private String dateOrdered;
	public static final int MAX_LIMITTED_ORDERS = 5;
	private static int nbOrders = 0;

	public Order() {
		super();
		nbOrders++;
	}

	public static int getNbOrders() {
		return nbOrders;
	}

	public String getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(String dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public void addMedia(Media media) {
		itemsOrdered.add(media);
		System.out.println("New entry added.");
	}

	public void removeMedia(int id) {
		int check = 0;
		if (itemsOrdered.isEmpty()) {
			System.out.println("Order list is empty.");
		} else {
			for (int i = 0; i < itemsOrdered.size(); i++) {
				if (itemsOrdered.get(i).getId() == id) {
					itemsOrdered.remove(i);
					check = 1;
					System.out.println("Entry removed.");
				}
			}
			if (check == 0) {
				System.out.println("Could not find the item with id " + id);
			}
		}
	}

	public Media getALuckyItem() {
		Random r = new Random();
		int randomItem = r.nextInt(itemsOrdered.size());
		return itemsOrdered.get(randomItem);
	}

	public double totalCost() {
		double totalCost = 0;
		Media lucky = getALuckyItem();
		for (int i = 0; i < itemsOrdered.size(); i++) {
			if (itemsOrdered.get(i) != lucky) {
				totalCost += itemsOrdered.get(i).getCost();
			} else {
				luckyIndex = i;
			}
		}
		return totalCost;
	}

	public void print() {
		System.out.println("*********************Order*********************");
		System.out.println("Date: " + getDateOrdered());
		System.out.println("Ordered Items:");
		for (int i = 0; i < itemsOrdered.size(); i++) {
			if (i != luckyIndex) {
				System.out.println(i + ". " + "Media - Name: " + itemsOrdered.get(i).getTitle() + " - ID: "
						+ itemsOrdered.get(i).getId() + " - Category: " + itemsOrdered.get(i).getCategory() + " - Price"
						+ ": " + itemsOrdered.get(i).getCost());
			} else {
				System.out.println(i + ". " + "Media - Name: " + itemsOrdered.get(i).getTitle() + " - ID: "
						+ itemsOrdered.get(i).getId() + " - Category: " + itemsOrdered.get(i).getCategory() + " - Price"
						+ ": " + "Free (lucky item)");
			}
		}
		System.out.println("Total cost: " + totalCost());
		System.out.println("***********************************************");
	}
}
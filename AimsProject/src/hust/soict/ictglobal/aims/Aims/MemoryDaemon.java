package hust.soict.ictglobal.aims.Aims;

public class MemoryDaemon implements java.lang.Runnable {
	long memoryUsed = 0;

	public void run() {
		Runtime runtime = Runtime.getRuntime();
		long memUsed;

		while (true) {
			memUsed = runtime.totalMemory() - runtime.freeMemory();
			if (memUsed != memoryUsed) {
				System.out.println("\tMemory used = " + memUsed);
				memoryUsed = memUsed;
			}
		}
	}
}

package hust.soict.ictglobal.aims.Aims;

import java.util.Scanner;

import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.CompactDisc;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Track;
import hust.soict.ictglobal.aims.order.Order.Order;

public class Aims {
	public static void main(String[] args) throws PlayerException {
		MemoryDaemon mem = new MemoryDaemon();
		Thread thread = new Thread(mem);
		thread.setDaemon(true);
		thread.start();
		Scanner scanner = new Scanner(System.in);
		Order anOrder = new Order();
		int opt;
		do {
			System.out.println("Order Management Application: ");
			System.out.println("--------------------------------");
			System.out.println("1. Create new order");
			System.out.println("2. Add item to the order");
			System.out.println("3. Delete item by id");
			System.out.println("4. Display the items list of order");
			System.out.println("0. Exit");
			System.out.println("--------------------------------");
			System.out.println("Please choose a number: 0-1-2-3-4");
			opt = scanner.nextInt();
			switch (opt) {
			case 1:
				anOrder = new Order();
				System.out.println("New order created.");
				break;

			case 2:
				int type;
				do {
					System.out.println("Enter media type (1 - Book, 2 - CompactDisc, 3 - DigitalVideoDisc):");
					type = scanner.nextInt();
					switch (type) {
					case 1:
						System.out.println("Enter book id :");
						int id = scanner.nextInt();
						scanner.nextLine();
						System.out.println("Enter book title: ");
						String title = scanner.nextLine();
						System.out.println("Enter book price: ");
						float price = scanner.nextFloat();
						scanner.nextLine();
						System.out.println("Enter book category: ");
						String category = scanner.nextLine();
						Book book1 = new Book();
						book1.setCategory(category);
						book1.setCost(price);
						book1.setId(id);
						book1.setTitle(title);
						anOrder.addMedia(book1);
						break;
					case 2:
						CompactDisc CD1 = new CompactDisc();
						System.out.println("Enter CD id :");
						int id2 = scanner.nextInt();
						scanner.nextLine();
						System.out.println("Enter CD title: ");
						String title2 = scanner.nextLine();
						System.out.println("Enter CD price: ");
						float price2 = scanner.nextFloat();
						scanner.nextLine();
						System.out.println("Enter CD category: ");
						String category2 = scanner.nextLine();
						System.out.println("Enter CD artist: ");
						String artist = scanner.nextLine();
						int check = 0;
						int check2 = 0;
						do {
							System.out.println("Enter 1 to add a new track, 2 to continue the program.");

							check = scanner.nextInt();
							if (check != 1 && check != 2) {
								System.out.println("Invalid input!");
							}
							if (check == 1) {
								Track track1 = new Track();
								System.out.println("Enter the title of the track: ");
								String titleTr = scanner.nextLine();
								track1.setTitle(titleTr);
								System.out.println("Enter the length of the track: ");
								int length = scanner.nextInt();
								track1.setLength(length);
								CD1.addTrack(track1);
							}

						} while (check != 2);
						CD1.setArtist(artist);
						CD1.setCategory(category2);
						CD1.setId(id2);
						CD1.setTitle(title2);
						CD1.setCost(price2);
						anOrder.addMedia(CD1);
						do {
							System.out.println("Play this CD?");
							System.out.println("1. Yes");
							System.out.println("2. No");
							check2 = scanner.nextInt();
							if (check2 == 1) {
								try {
									CD1.play();
								} catch (PlayerException exp) {
									exp.getMessage();
									exp.toString();
									exp.printStackTrace();
								}
							} else if (check2 == 2) {
								System.out.println("OK.");
							} else {
								System.out.println("Invalid input!");
							}
						} while (check2 != 1 && check2 != 2);
						break;

					case 3:
						int check3 = 0;
						System.out.println("Enter DVD id :");
						int id3 = scanner.nextInt();
						scanner.nextLine();
						System.out.println("Enter DVD title: ");
						String title3 = scanner.nextLine();
						System.out.println("Enter DVD price: ");
						float price3 = scanner.nextFloat();
						scanner.nextLine();
						System.out.println("Enter DVD category: ");
						String category3 = scanner.nextLine();
						System.out.println("Enter DVD director: ");
						String director = scanner.nextLine();
						System.out.println("Enter DVD length: ");
						int length = scanner.nextInt();
						DigitalVideoDisc disc1 = new DigitalVideoDisc(title3, category3, director, length, price3);
						disc1.setId(id3);
						anOrder.addMedia(disc1);
						do {
							System.out.println("Play this DVD?");
							System.out.println("1. Yes");
							System.out.println("2. No");
							check3 = scanner.nextInt();
							if (check3 == 1) {
								try {
									disc1.play();
								} catch (PlayerException exp) {
									exp.getMessage();
									exp.toString();
									exp.printStackTrace();
								}
							} else if (check3 == 2) {
								System.out.println("OK.");
							} else {
								System.out.println("Invalid input!");
							}
						} while (check3 != 1 && check3 != 2);
						break;

					default:
						System.out.println("Invalid input!");
					}
				} while (type != 1 && type != 2 && type != 3);
				break;

			case 3:
				System.out.println("Enter media id to be removed: ");
				int i = scanner.nextInt();
				anOrder.removeMedia(i);
				break;

			case 4:
				anOrder.print();
				break;

			case 0:
				System.out.println("Exiting...");
				System.exit(0);
				break;

			default:
				System.out.println("Invalid input. Try again.");
			}
		} while (opt != 0);
		scanner.close();
	};

}